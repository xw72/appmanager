import React, { Component } from 'react';
import axios from 'axios';
import Config from '../Config.jsx';
import AuthStore from '../../stores/AuthStore.jsx';
import AppActions from '../../actions/AppActions.jsx';
import AppStore from '../../stores/AppStore.jsx';

class UserAppNew extends Component {
  constructor(props) {
    super(props);

    this.state = {
      permissionObj: Config.getPermissionObj(),
      validationErrors: {},
      appIds: [],
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onRedirectURIsChange = this.onRedirectURIsChange.bind(this);
    this.onAddRedirectURI = this.onAddRedirectURI.bind(this);
    this.validateClientId = this.validateClientId.bind(this);
    this.validatePrivacyURL = this.validatePrivacyURL.bind(this);

    this.updatePermissions = this.updatePermissions.bind(this);
    this.permissions = [{
      service: 'basic',
      access: 'full',
    }];
  }

  componentDidMount() {
    if (Object.keys(this.state.permissionObj).length === 0 && JSON.stringify(this.state.permissionObj) === JSON.stringify({})) {
      this.state.permissionObj.basic = 'Basic OAuth Permissions';
      axios
        .get('https://api.colab.duke.edu/meta/v1/apis', {
          headers: {
            'Content-Type': 'application/json',
            'x-api-key': Config.getClientId(),
          },
        })
        .then((result) => {
          const apis = [];
          const versions = [];
          for (const key of Object.keys(result.data)) {
            apis.push(key);
            const versionsArray = result.data[key];
            versions.push(versionsArray[versionsArray.length - 1]);
          }

          axios
            .get('https://api.colab.duke.edu/meta/v1/docs', {
              headers: {
                'Content-Type': 'application/json',
                'x-api-key': Config.getClientId(),
              },
            })
            .then((res) => {
              for (let i = 0; i < apis.length; i += 1) {
                const duke_auth = res.data[apis[i]][versions[i]].securityDefinitions.duke_auth;
                if (duke_auth !== undefined && duke_auth.scopes !== undefined) {
                  for (const key of Object.keys(duke_auth.scopes)) {
                    this.state.permissionObj[key] = duke_auth.scopes[key];
                  }
                  this.setState({
                    permissionObj: this.state.permissionObj,
                  });
                  Config.setPermissionObj(this.state.permissionObj);
                }
              }
            })
            .catch((err) => {
              AppActions.handleError({ type: 'permission_error', body: err });
            });
        })
        .catch((error) => {
          AppActions.handleError({ type: 'permission_error', body: error });
        });
    }
  }

  onRedirectURIsChange(idx) {
    if (idx) {
      if (idx === 0) {
        throw new Error('Do not change the mandatory URI!');
      }
      const { activeUserApp } = AppStore.getState();
      activeUserApp.redirectURIs[idx] = this[`_redirectURIs${idx}`].value;
      AppActions.syncActiveUserApp(activeUserApp);
    }
  }

  onChange(e) {
    const { editing } = AppStore.getState();
    switch (e.target) {
      case this._displayName:
        AppActions.refreshNewAppName(e.target.value);
        if (!editing) {
          AppActions.syncClientId(e.target.value);
        }
        break;
      case this._clientId:
        AppActions.syncActiveUserApp({
          clientId: this._clientId.value,
        });
        break;
      case this._description:
        AppActions.syncActiveUserApp({
          description: this._description.value,
        });
        break;
      case this._ownerDescription:
        AppActions.syncActiveUserApp({
          ownerDescription: this._ownerDescription.value,
        });
        break;
      case this._privacyURL:
        AppActions.syncActiveUserApp({
          privacyURL: this._privacyURL.value,
        });
        break;
      default:
        break;
    }
  }

  onRedirectURIsBlur(idx) {
    this.validateRedirectURI(idx, this[`_redirectURIs${idx}`].value);
  }

  onBlur(e) {
    e.preventDefault();
    const { activeUserApp } = AppStore.getState();
    switch (e.target) {
      case this._clientId:
      case this._displayName:
        this.validateClientId(activeUserApp.clientId);
        break;
      case this._privacyURL:
        this.validatePrivacyURL(activeUserApp.privacyURL);
        break;
      default:
        break;
    }
  }

  onCancel(e) {
    e.preventDefault();
    AppActions.editApp(false);
  }

  onAddRedirectURI(e) {
    e.preventDefault();
    const { activeUserApp } = AppStore.getState();
    activeUserApp.redirectURIs.push('');
    AppActions.syncActiveUserApp(activeUserApp);
  }

  onDeleteRedirectURI(idx) {
    const { activeUserApp } = AppStore.getState();
    activeUserApp.redirectURIs.splice(idx, 1);
    AppActions.syncActiveUserApp(activeUserApp);
  }

  onSubmit(e) {
    e.preventDefault();

    const { activeUserApp, editing, activeUserAppBackup } = AppStore.getState();
    const permissions = this.permissions;

    this.validateClientId(activeUserApp.clientId);
    activeUserApp.redirectURIs.forEach((item, idx) => {
      this.validateRedirectURI(idx, item);
    });
    this.validatePrivacyURL(activeUserApp.privacyURL);

    if (Object.keys(this.state.validationErrors).length === 0) {
      let activeUserAppReq = null;
      if (!editing) {
        activeUserAppReq = {
          clientId: activeUserApp.clientId,
          redirectURIs: activeUserApp.redirectURIs,
          displayName: activeUserApp.displayName,
          description: activeUserApp.description,
          ownerDescription: activeUserApp.ownerDescription,
          privacyURL: activeUserApp.privacyURL,
          permissions,
        };
      } else {
        activeUserAppReq = {
          clientId: activeUserAppBackup.clientId,
          clientSecret: activeUserApp.clientSecret,
          redirectURIs: activeUserApp.redirectURIs,
          displayName: activeUserApp.displayName,
          description: activeUserApp.description,
          ownerDescription: activeUserApp.ownerDescription,
          privacyURL: activeUserApp.privacyURL,
          permissions,
          appOwners: activeUserApp.appOwners,
        };
      }

      AppActions.submitUserApp(activeUserAppReq);
    }
  }

  validateClientId(clientId) {
    const { validationErrors } = this.state;
    delete validationErrors.clientId;
    // clientId check
    if (!/^[a-z0-9-]+$/i.test(clientId)) {
      validationErrors.clientId = {
        title: 'Invalid client ID',
        body: clientId,
      };
    }

    axios
      .get('https://api.colab.duke.edu/meta/v1/apps?all=true', {
        headers: {
          'Content-Type': 'application/json',
          'x-api-key': Config.getClientId(),
          Authorization: `Bearer ${AuthStore.getState().accessToken}`,
          Accept: 'application/json',
        },
      })
      .then((res) => {
        const { appIds } = this.state;
        for (const app of res.data) {
          if (Object.keys(app).length !== 0 && JSON.stringify(app) !== JSON.stringify({})) {
            if (appIds.indexOf(app.clientId) === -1) {
              appIds.push(app.clientId);
            }
          }
        }
        if (appIds.indexOf(clientId) !== -1) {
          validationErrors.clientId = {
            title: 'client ID is already taken',
            body: clientId,
          };
        }
        this.setState({
          validationErrors,
          appIds,
        });
      })
      .catch((err) => {
        AppActions.handleError({ type: 'application_error', body: err });
      });

    this.setState({
      validationErrors,
    });
  }

  validateRedirectURI(index, uri) {
    const { validationErrors } = this.state;
    delete validationErrors[`redirectURIs${index}`];
    if (!(Config.getURLRegex().test(uri) || Config.getLocalhostRegex().test(uri))) {
      validationErrors[`redirectURIs${index}`] = {
        title: 'Invalid URI',
        body: uri,
      };
    }

    this.setState({
      validationErrors,
    });
  }

  validatePrivacyURL(privacyURL) {
    const { validationErrors } = this.state;
    delete validationErrors.privacyURL;
    if (!(Config.getURLRegex().test(privacyURL) || Config.getLocalhostRegex().test(privacyURL))) {
      validationErrors.privacyURL = {
        title: 'Invalid Privacy URL',
        body: privacyURL,
      };
    }

    this.setState({
      validationErrors,
    });
  }

  updatePermissions(e) {
    if (e.target.checked) {
      this.permissions.push({
        service: e.target.value,
        access: 'full',
      });
    } else {
      this.permissions = this.permissions.filter((elem) => {
        return elem.service !== e.target.value;
      });
    }
  }

  render() {
    const { permissionObj, validationErrors } = this.state;
    const { activeUserApp, editing } = AppStore.getState();
    return (
      <div className="newAppForm">
        {Object.keys(validationErrors).map((errKey) => {
          return (
            <div className="panel panel-danger" key={errKey}>
              <div className="panel-heading">
                <h3 className="panel-title">{validationErrors[errKey].title}</h3>
              </div>
              <div className="panel-body">
                <p>{validationErrors[errKey].body}</p>
              </div>
            </div>
          );
        })}
        <form onSubmit={this.onSubmit} className="form-horizontal">
          <div className="form-group">
            <label htmlFor="displayName" className="col-sm-3 control-label">App Name</label>
            <div className="col-sm-8">
              <input className="col-sm-12" type="text" required ref={(r) => { this._displayName = r; }} value={activeUserApp.displayName} onChange={this.onChange} onBlur={this.onBlur} aria-describedby="displayNameHelp" />
              <span id="displayNameHelp" className="help-block">The name of your application</span>
            </div>
          </div>

          <div className={this.state.validationErrors.clientId ? 'form-group has-error has-feedback' : 'form-group'}>
            <label htmlFor="clientId" className="col-sm-3 control-label">Client ID</label>
            <div className="col-sm-8">
              <input className="col-sm-12 form-control" type="text" required ref={(r) => { this._clientId = r; }} value={activeUserApp.clientId} onChange={this.onChange} onBlur={this.onBlur} aria-describedby="ClientIDHelp" disabled={editing} />
              <span className={this.state.validationErrors.clientId ? 'glyphicon glyphicon-remove form-control-feedback' : 'hidden'} aria-hidden="true" />
              <span id="ClientIDHelp" className="help-block">This is a simplified version of your app name, without spaces or odd characters.</span>
            </div>
          </div>

          {activeUserApp.redirectURIs.map((item, idx, arr) => {
            if (item !== 'http://apidocs.colab.duke.edu/o2c.html') {
              const boundChange = this.onRedirectURIsChange.bind(this, idx);
              const boundBlur = this.onRedirectURIsBlur.bind(this, idx);
              const boundDelete = this.onDeleteRedirectURI.bind(this, idx);
              if (idx === 1 && arr.length === 2) {
                return (
                  <div key={idx} className={this.state.validationErrors[`redirectURIs${idx}`] ? 'form-group has-error has-feedback' : 'form-group'}>
                    <label htmlFor="redirectURIs" className="col-sm-3 control-label">
                      Redirect URI
                      <br />
                      <a onClick={this.onAddRedirectURI} className="small">Add an redirect URI</a>
                    </label>
                    <div className="col-sm-8">
                      <input name="redirectURIs" className="col-sm-12" type="text" onBlur={boundBlur} ref={(r) => { this[`_redirectURIs${idx}`] = r; }} value={item} onChange={boundChange} aria-describedby="RedirectURIsHelp" required />
                      <span className={this.state.validationErrors.redirectURIs ? 'glyphicon glyphicon-remove form-control-feedback' : 'hidden'} aria-hidden="true" />
                      <span id="RedirectURIsHelp" className="help-block">This is where the OAuth system redirects a user after authentication.  Please use http(s):// in your URI above.</span>
                    </div>
                  </div>
                );
              } else if (idx === 1 && arr.length > 2) {
                return (
                  <div key={idx} className={this.state.validationErrors[`redirectURIs${idx}`] ? 'form-group has-error has-feedback' : 'form-group'}>
                    <label htmlFor="redirectURIs" className="col-sm-3 control-label">
                      Redirect URI
                      <br />
                      <a onClick={this.onAddRedirectURI} className="small">Add an redirect URI</a>
                    </label>
                    <div className="col-sm-8">
                      <input name="redirectURIs" className="col-sm-12" type="text" onBlur={boundBlur} ref={(r) => { this[`_redirectURIs${idx}`] = r; }} value={item} onChange={boundChange} aria-describedby="RedirectURIsHelp" required />
                      <span className={this.state.validationErrors.redirectURIs ? 'glyphicon glyphicon-remove form-control-feedback' : 'hidden'} aria-hidden="true" />
                      <span id="RedirectURIsHelp" className="help-block">This is where the OAuth system redirects a user after authentication.  Please use http(s):// in your URI above.</span>
                    </div>
                    <div className="col-sm-1 plus-button">
                      <span className="glyphicon glyphicon-remove" onClick={boundDelete} />
                    </div>
                  </div>
                );
              }
              return (
                <div key={idx} className={this.state.validationErrors[`redirectURIs${idx}`] ? 'form-group has-error has-feedback' : 'form-group'}>
                  <label htmlFor="redirectURIs" className="col-sm-3 control-label" />
                  <div className="col-sm-8">
                    <input name="redirectURIs" className="col-sm-12" type="text" onBlur={boundBlur} ref={(r) => { this[`_redirectURIs${idx}`] = r; }} value={item} onChange={boundChange} aria-describedby="RedirectURIsHelp" required />
                    <span className={this.state.validationErrors.redirectURIs ? 'glyphicon glyphicon-remove form-control-feedback' : 'hidden'} aria-hidden="true" />
                    <span id="RedirectURIsHelp" className="help-block">This is where the OAuth system redirects a user after authentication.  Please use http(s):// in your URI above.</span>
                  </div>
                  <div className="col-sm-1 plus-button">
                    <span className="glyphicon glyphicon-remove" onClick={boundDelete} />
                  </div>
                </div>
              );
            }
            return (
              <div key={idx} className="form-group" id="apidocsURI">
                <label htmlFor="apidocsURI" className="col-sm-3 control-label">API Docs <br /> Redirect URI</label>
                <div className="col-sm-8">
                  <input name="apidocsURI" className="col-sm-12 form-control" type="text" ref={(r) => { this[`_redirectURIs${idx}`] = r; }} value={item} aria-describedby="RedirectURIsHelp" disabled />
                  <span id="RedirectURIsHelp" className="help-block">This is the mandatory redirect URI for any application to explore the Co-Lab API documentations.</span>
                </div>
              </div>
            );
          })}

          <div className="form-group">
            <label htmlFor="description" className="col-sm-3 control-label">App Description</label>
            <div className="col-sm-8">
              <textarea className="col-sm-12" rows="2" required ref={(r) => { this._description = r; }} value={activeUserApp.description} onChange={this.onChange} aria-describedby="descriptionHelp" />
              <span id="descriptionHelp" className="help-block">Tell us a bit about your app and which data sources you're planning to use.</span>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="ownerDescription" className="col-sm-3 control-label">Owner Description</label>
            <div className="col-sm-8">
              <textarea className="col-sm-12" rows="2" required ref={(r) => { this._ownerDescription = r; }} value={activeUserApp.ownerDescription} onChange={this.onChange} aria-describedby="ownerDescriptionHelp" />
              <span id="ownerDescriptionHelp" className="help-block">Who you are / what group this project is for.</span>
            </div>
          </div>

          <div className={this.state.validationErrors.privacyURL ? 'form-group has-error has-feedback' : 'form-group'}>
            <label htmlFor="privacyURL" className="col-sm-3 control-label">Privacy URL</label>
            <div className="col-sm-8">
              <input className="col-sm-12" type="text" required ref={(r) => { this._privacyURL = r; }} value={activeUserApp.privacyURL} onChange={this.onChange} onBlur={this.onBlur} aria-describedby="privacyURLHelp" />
              <span className={this.state.validationErrors.privacyURL ? 'glyphicon glyphicon-remove form-control-feedback' : 'hidden'} aria-hidden="true" />
              <span id="privacyURLHelp" className="help-block">Info about Privacy URL stuffs</span>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="permissions" className="col-sm-3 control-label">Permissions</label>
            <div className="col-sm-8" id="permission-matrix">
              {Object.keys(permissionObj).map((perm) => {
                if (perm === 'basic') {
                  return (
                    <div className="checkbox disabled form-control" key={perm}>
                      <input type="checkbox" disabled="true" checked="true" /> {permissionObj[perm]}
                    </div>
                  );
                }
                return (
                  <div className="checkbox form-control" key={perm}>
                    <input type="checkbox" value={perm} onClick={this.updatePermissions} /> {permissionObj[perm]}
                  </div>
                );
              })}
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-offset-3 col-sm-8">
              { editing ? <button onClick={this.onCancel} className="btn btn-danger form-buttons form-cancel">Cancel</button> : null }
              <button id="submit-button" type="submit" className="btn btn-success form-buttons">Submit</button>
            </div>
          </div>

        </form>
      </div>
    );
  }
}

export default UserAppNew;
