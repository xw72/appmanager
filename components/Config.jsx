import querystring from 'querystring';

class Config {
  static getClientId() {
    return 'innovation-co-lab-app-manager';
    // return 'app-manager-dev';
  }

  static getUserApps() {
    if (typeof sessionStorage !== 'undefined') {
      return JSON.parse(sessionStorage.getItem('userApps')) || [];
    }
    return [];
  }

  static getPermissionObj() {
    if (typeof sessionStorage !== 'undefined') {
      return JSON.parse(sessionStorage.getItem('permissionObj')) || {};
    }
    return [];
  }

  static setPermissionObj(obj) {
    sessionStorage.setItem('permissionObj', JSON.stringify(obj));
  }

  static getState() {
    return '842867';
  }

  static getQueryString() {
    return querystring.stringify({
      response_type: 'token',
      redirect_uri: 'https://appmanager.colab.duke.edu',
      client_id: 'innovation-co-lab-app-manager',
      // redirect_uri: 'http://localhost:3001',
      // client_id: 'app-manager-dev',
      scope: 'meta:api:write meta:apps:write meta:apps:read oauth_registrations identity:netid:read',
      state: '842867',
    });
  }

  static getURLRegex() {
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/;
  }

  static getLocalhostRegex() {
    return /^https?:\/\/\localhost(:[0-9]+)?\/?(\/[.\w]*)*$/;
  }

  static getDefaultPrivacyURL() {
    return 'https://dev.colab.duke.edu/#privacy';
  }
}

export default Config;
